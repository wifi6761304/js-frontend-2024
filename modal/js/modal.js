/*
	IIFE Immediately invoked function expression
	wir erstellen eine anonyme Funktion, die sich gleich selbst aufruft -> wir erzeugen dadurch
	einen lokalen scope für unser Script.

	Wir erzeugen eine Expression/Ausdruck durch eine Klammerung der anonymen Funktions Deklaration.
	(function_deklaration())
	Durch die Klammern wird die Funktion deklariert und kann sofort ausgeführt werden.
	Alternative Schreibweise

	!function(){...}()
	BEST PRACTICE:
	(function(args){...}())

*/

// Dialog öffnen
function onModalOpen(e) {
	modal.classList.add("show");
}

// Dialog schließen
function onModalClose(e) {
	modal.classList.remove("show");
}

// DOM Elemente
const modal = document.getElementById("modal");
const buttons = document.querySelectorAll(".btn-modal");
const modalBtnClose = modal.querySelector(".modal-btn-close");

// Event Listener für alle modal open buttons
buttons.forEach((btn) => {
	btn.addEventListener("click", onModalOpen);
});

// Event Listener für modal close button
modalBtnClose.addEventListener("click", onModalClose);

/*
	TODO:
	- mehrere modal close buttons ermöglichen
	- schließen bei click auf overlay
	- schließen bei ESC
	- animieren des Öffnen und Schließen des modals
*/
